# lightdm-webkit2-c3c

[Link to Demo-Video](https://chaos.social/system/media_attachments/files/005/219/929/original/58163f3c08db4088.mp4)


This is a simple greeter for lightdm-webkit2, with a countdown to the next Chaos Communication Congress. 

## How to install and setup on Arch Linux

you need the [webkit2 greeter](https://www.archlinux.org/packages/community/x86_64/lightdm-webkit2-greeter/) for lightdm

```
pacman -S lightdm-webkit2-greeter
```

then you can clone this repo (you need root privileges to clone this repo directly into the theme forlder)

```
git clone https://git.elektrollart.org/Elektroll/lightdm-webkit2-c3c.git /usr/share/lightdm-webkit/themes/c3c
```

Edit **/etc/lightdm/lightdm.conf** and add this line, if not exists:


```
[Seat:*]
greeter-session = lightdm-webkit2-greeter
```

Set the the theme which lightdm-webkit2-greeter should use in **/etc/lightdm/lightdm-webkit2-greeter.conf** :

```
webkit_theme        = c3c
```

c3c is the foldername. 

Edit LightDM config file **/etc/lightdm/lightdm.conf**
```
[LightDM]
minimum-vt=7 # Setting this to a value < 7 implies security issues, see FS#46799
log-directory=/var/log/lightdm
run-directory=/run/lightdm
sessions-directory=/usr/share/lightdm/sessions:/usr/share/xsessions:/usr/share/wayland-sessions


[Seat:*]
greeter-session = lightdm-webkit2-greeter
pam-service=lightdm
user-session=1hikari
session-wrapper=/etc/lightdm/Xsession

[XDMCPServer]

[VNCServer]
```

Don't allow X11 to switch Virtual Terminal, because you can switch the VT and so jump into a running session.
Create a xorg config file into **/etc/X11/xorg.conf.d/00-blockvt.conf**

```
Section "ServerFlags"
    Option "DontVTSwitch" "on"
EndSection
```

If you use a different dm, like GDM from gnome, disable this service 

```
systemctl stop gdm
systemctl disable gdm
```

and start lightdm

```
systemctl start lightdm
```

if all works fine and you can login, enable lightdm


```
systemctl enable lightdm
```